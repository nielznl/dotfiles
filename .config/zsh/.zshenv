XDG_CONFIG_HOME=$HOME/.config
XDG_CACHE_HOME=$HOME/.cache
XDG_DATA_HOME=$HOME/.local/share
XDG_STATE_HOME=$HOME/.local/state
ZDOTDIR=$HOME/.config/zsh # /etc/zsh/zshenv
HISTFILE="$XDG_STATE_HOME"/zsh/history
mkdir -p "$XDG_STATE_HOME"/zsh
GNUPGHOME="$XDG_DATA_HOME"/gnupg

EDITOR=nvim
