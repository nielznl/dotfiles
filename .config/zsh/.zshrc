DISABLE_MAGIC_FUNCTIONS=true
TYPEWRITTEN_PROMPT_LAYOUT="singleline_verbose"
TYPEWRITTEN_ARROW_SYMBOL="➜"
TYPEWRITTEN_RELATIVE_PATH="adaptive"
TYPEWRITTEN_CURSOR="terminal" # beam


source /usr/share/zsh/share/antigen.zsh

antigen bundle reobin/typewritten@main

antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle supercrabtree/k

antigen use oh-my-zsh
antigen bundle command-not-found
antigen bundle z
antigen bundle virtualenvwrapper
antigen bundle aliases
antigen bundle pip
antigen bundle docker
antigen bundle docker-compose

antigen apply


path+=("$HOME/.local/bin")
export PATH

source /usr/share/LS_COLORS/dircolors.sh # Alternatively https://github.com/twisty/dotfiles/blob/master/gruvbox.dircolors

bindkey -v
bindkey -M vicmd 'V' edit-command-line

[ "$TERM" = "xterm-kitty" ] && alias ssh="kitty +kitten ssh"
