#!/bin/bash
set -eu

declare -a extensions=(
    "3910071/bitwarden_free_password_manager-1.56.6-an+fx.xpi"
    "3922130/dark_reader-4.9.47-an+fx.xpi"
    "3910598/canvasblocker-1.8-an+fx.xpi"
    "3902154/decentraleyes-2.0.17-an+fx.xpi"
)

destination=/usr/lib/librewolf/browser/extensions
sudo mkdir -p $destination/downloads
for i in "${extensions[@]}"; do
    file_name=$(echo $i | sed 's/\//\n/g' | tail -n 1)
    dl_file="$destination/downloads/$file_name"
    url="https://addons.mozilla.org/firefox/downloads/file/$i"
    if [ ! -f "$dl_file" ]; then sudo wget -q "$url" -O "$dl_file"; fi
    temp_dir=$(mktemp -d)
    unzip -qq "$dl_file" -d $temp_dir
    new_file=$(jq -r '.applications.gecko.id' $temp_dir/manifest.json)
    #if [ "$new_file" = "null" ]; then
    #	new_file=$(jq -r '.browser_specific_settings.gecko.id' $temp_dir/manifest.json)
    #fi
    rm -r $temp_dir
    sudo cp "$dl_file" "$destination/$new_file.xpi"
done
