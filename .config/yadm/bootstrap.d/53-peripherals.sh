#!/bin/bash
set -eu

file=/etc/X11/xorg.conf.d/50-mouse-acceleration.conf
echo '''Section "InputClass"
    Identifier "My Mouse"
    MatchIsPointer "yes"
    Option "AccelerationProfile" "-1"
    Option "AccelerationScheme" "none"
    Option "AccelSpeed" "-1"
EndSection''' | sudo tee $file > /dev/null

file=/sys/module/hid_apple/parameters/fnmode
if [ -f "$file" ]; then
    echo 2 | sudo tee $file > /dev/null
fi  
echo options hid_apple fnmode=2 | sudo tee /etc/modprobe.d/hid_apple.conf > /dev/null
