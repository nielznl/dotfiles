#!/bin/bash
set -eu

sudo sed -i 's/#AutoEnable=false/AutoEnable=true/' /etc/bluetooth/main.conf
sudo systemctl enable --now -q bluetooth.service
