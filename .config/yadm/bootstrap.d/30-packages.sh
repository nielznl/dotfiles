#!/bin/bash
set -eu

mkdir -p $HOME/.config/newsboat

# Multilib
sudo sed -z 's/\#\[multilib\]\n\#/\[multilib\]\n/g' -i /etc/pacman.conf

# yay
sudo pacman -Suy --needed --noconfirm git base-devel &> /dev/null
if ! pacman -Qs yay &> /dev/null; then
    dir=$(mktemp -d)
    git clone https://aur.archlinux.org/yay.git $dir &> /dev/null
    (cd $dir && makepkg -si --noconfirm &> /dev/null)
fi

# Bootstrap requirements
echo 'Installing required packages'
yay --needed --noconfirm --sudoloop --norebuild --noredownload -S \
    inetutils \
    unzip \
    bluez \
    bluez-utils \
    jq \
    pkgfile \
    syncthing \
    wget \
    &> /dev/null

# Packages
echo 'Installing official packages'
yay --needed --noconfirm --sudoloop -S \
    bat \
    bitwarden \
    docker \
    docker-compose \
    gnome \
    gnome-extra \
    gnome-themes-extra \
    kitty \
    libreoffice \
    neofetch \
    neovim \
    net-tools \
    newsboat \
    nmap \
    python \
    python-pip \
    python-virtualenvwrapper \
    pwgen \
    remmina \
    signal-desktop \
    telegram-desktop \
    texlive-most \
    thunderbird \
    tree \
    ttf-cascadia-code \
    ttf-nerd-fonts-symbols-2048-em \
    xclip \
    zathura \
    zathura-pdf-mupdf \
    &> /dev/null

# Nonfree
yay --needed --noconfirm --sudoloop -S \
    steam \
    discord \
    &> /dev/null

# AUR packages
echo 'Installing AUR packages'
yay --needed --noconfirm --sudoloop --norebuild --noredownload -S \
    antigen-git \
    cider-git \
    librewolf-bin \
    lscolors-git \
    mattermost-desktop-bin \
    nvchad-git \
    teams-for-linux \
    vmware-horizon-client \
    vscodium-bin \
    &> /dev/null

# Python pip packages
pip install -q --no-warn-script-location \
    grip \
    python-openstackclient

# Remove unneeded packages
yay -Runs --noconfirm firefox &> /dev/null || true
yay -Runs --noconfirm alacritty &> /dev/null || true
