#!/bin/bash
set -eu

if [ -f ~/.config/monitors.xml ]; then
    sudo cp ~/.config/monitors.xml /var/lib/gdm/.config
    sudo chown gdm:gdm /var/lib/gdm/.config/monitors.xml
fi


sudo sed 's/\#WaylandEnable=false/WaylandEnable=false/' -i /etc/gdm/custom.conf

gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/gnome/blobs-l.svg'
gsettings set org.gnome.desktop.background picture-uri-dark 'file:///usr/share/backgrounds/gnome/blobs-d.svg'
gsettings set org.gnome.desktop.calendar show-weekdate true
gsettings set org.gnome.desktop.interface clock-show-seconds true
gsettings set org.gnome.desktop.interface clock-show-weekday true
gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'
gsettings set org.gnome.desktop.interface show-battery-percentage true
gsettings set org.gnome.desktop.peripherals.keyboard delay 200
gsettings set org.gnome.desktop.peripherals.keyboard repeat-interval 30
gsettings set org.gnome.desktop.peripherals.mouse accel-profile 'flat'
gsettings set org.gnome.desktop.peripherals.touchpad disable-while-typing false
gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
gsettings set org.gnome.desktop.screensaver picture-uri 'file:///usr/share/backgrounds/gnome/blobs-l.svg'
gsettings set org.gnome.gedit.preferences.editor scheme 'solarized-dark'
gsettings set org.gnome.settings-daemon.plugins.power power-button-action 'hibernate'
gsettings set org.gnome.shell favorite-apps "['discord.desktop', 'librewolf.desktop', 'org.remmina.Remmina.desktop', 'kitty.desktop', 'cider.desktop', 'mattermost-desktop.desktop', 'bitwarden.desktop', 'org.gnome.Nautilus.desktop']"
