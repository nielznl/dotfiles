#!/bin/bash
set -eu

sudo usermod -aG uucp,docker,input,adm $USER
