#!/bin/bash
set -eu

zshPath=/usr/bin/zsh
if [ "$(echo $SHELL)" != "$zshPath" ]; then relog=1; fi
sudo chsh -s $zshPath $USER &> /dev/null
sudo pkgfile -u > /dev/null
sudo systemctl enable -q pkgfile-update.timer
echo 'ZDOTDIR=$HOME/.config/zsh' | sudo tee /etc/zsh/zshenv > /dev/null
