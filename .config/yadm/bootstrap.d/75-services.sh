#!/bin/bash
set -eu

sudo systemctl enable --now -q docker
sudo systemctl enable --now -q syncthing@$USER.service
sudo systemctl enable -q gdm
echo 'Syncthing may be configured at http://127.0.0.1:8384/'
