## Dotfiles & bootstrap scripts

Dotfiles and bootstrap scripts to synchronize the installation and configuration of various packages across multiple devices.

### Linux installation
EndeavourOS can easily be used to install Arch Linux by unchecking `EndeavourOS apps` and `EndeavourOS applications selection` under `Base-devel + Common packages` during the installation.

### Clone dotfiles and optionally run bootstrap scripts
```
sudo pacman -S yadm
yadm clone https://gitlab.com/nielznl/dotfiles.git
```
